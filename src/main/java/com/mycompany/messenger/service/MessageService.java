
package com.mycompany.messenger.service;

import com.mycompany.messenger.databese.databaseClass;
import com.mycompany.messenger.model.Message;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

public class MessageService {
    
    private Map<Long,Message> messages = databaseClass.getMessages();
    public MessageService(){
        messages.put(1L, new Message(1," Zdravo", "MArko"));
        messages.put(2L, new Message(2," Zdravo", "Mica"));
        messages.put(3L, new Message(3," Oprem", "Dobro"));
    }
    
    
    public List<Message> getAllMessages(){
        return new ArrayList<Message>(messages.values());
    }
    public List<Message> getAllMessageForYear(int year){
        System.out.println("ULAZ");
        List<Message> messegForYear = new ArrayList<Message>();
        Calendar cal=Calendar.getInstance();
        System.out.println(cal.toString()+" ovo je kal");
        for(Message message : messages.values()){
            System.out.println("Ulaz u FOR" );
            System.out.println(" OVO je Created"+message.getCreated());
            cal.setTime(message.getCreated());
             System.out.println(message.getCreated() + "ovo je message");
            if(cal.get(Calendar.YEAR)== year){
                System.out.println("Dodato");
                messegForYear.add(message);
            }
        }
        return messegForYear;
    }
    public List<Message> getAllMessagePaginated(int start , int size){
        ArrayList<Message> list= new ArrayList<Message>(messages.values());
        if(start+size > list.size()) return new ArrayList<Message>();
        return list.subList(start , start + size);
    }
    
    
    public Message getMessage(Long id){
        return messages.get(id);
    }
    public Message addMessage(Message message){
        message.setId(messages.size()+1);
        messages.put(message.getId(), message);
        return message;
    }
    public Message updateMessage(Message message){
        if(message.getId()<=0){
            return null;
        }
        messages.put(message.getId(), message);
        return message;
    }
    public Message removeMessage(Long id){
        return messages.remove(id);
    }
}
