
package com.mycompany.messenger.service;

import com.mycompany.messenger.databese.databaseClass;
import com.mycompany.messenger.model.Profile;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ProfileService {
    
    private Map<String,Profile> profiles =databaseClass.getProfile();

    public ProfileService() {
        profiles.put("Mare", new Profile("Mare", "Marko", "Marinkovic",1L));
    }
    
    
    
    public List<Profile>getAllProfiles(){
        return new ArrayList<Profile>(profiles.values());
    }
    public Profile getProfil(String profil){
      return  profiles.get(profil);
    }
    public Profile addProfile(Profile profile){
        profile.setId(profiles.size()+1L);
        profiles.put(profile.getProfilName(), profile);
        return profile;
    }
    public Profile updateProfile(Profile profile){
        if(profile.getProfilName().isEmpty()){
            return null;
        }
        profiles.put(profile.getProfilName(), profile);
        return profile;
    }
    public Profile removeProfile(String profile){
      return  profiles.remove(profile);
    }
}
