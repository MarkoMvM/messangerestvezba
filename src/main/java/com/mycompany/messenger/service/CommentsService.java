
package com.mycompany.messenger.service;

import com.mycompany.messenger.databese.databaseClass;
import com.mycompany.messenger.model.Comment;
import com.mycompany.messenger.model.Message;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class CommentsService {
    private Map<Long,Message> listMessage = databaseClass.getMessages();
    
    public List<Comment> getAllComments(Long messageId){
        Map<Long,Comment> comments = listMessage.get(messageId).getComment();
        return new ArrayList<Comment>(comments.values());
    }
    
    public Comment getComment(long commentId,long messageId){
        Map<Long,Comment> comment = listMessage.get(messageId).getComment();
        return comment.get(commentId);
    }
    
    public Comment addComment(long messageId,Comment comment){
        Map<Long,Comment> comments = listMessage.get(messageId).getComment();
        comment.setId(comments.size()+1);
        comments.put(comment.getId(), comment);
        return comment;
}
    public Comment updateComment(long messageId,Comment comment){
        Map<Long,Comment> comments = listMessage.get(messageId).getComment();
        if(comment.getId()<=0){
            return null;
        }
        comments.put(comment.getId(), comment);
        return comment;
    }
    public Comment removeComment(long commentId,long messageId){
        Map<Long,Comment> comments = listMessage.get(messageId).getComment();
        return comments.remove(commentId);
    }
    
}