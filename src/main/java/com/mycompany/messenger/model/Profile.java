
package com.mycompany.messenger.model;

import java.util.Date;

public class Profile {
    private String profilName;
    private String frstName;
    private String lastName;
    private String created;
    private Long id;

    

    public Profile() {
    }

    public Profile(String profilNamep, String frstName, String lastName,Long id) {
        this.profilName = profilNamep;
        this.frstName = frstName;
        this.lastName = lastName;
        this.created = new Date().toString();
        this.id=id;
        
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    public String getProfilName() {
        return profilName;
    }

    public void setProfilName(String profilNamep) {
        this.profilName = profilNamep;
    }

    public String getFrstName() {
        return frstName;
    }

    public void setFrstName(String frstName) {
        this.frstName = frstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }
    
    
}
