
package com.mycompany.messenger.model;

import java.util.Date;

public class Comment {
  private String message;
  private  long id;
  private  Date created;
  private String autor;

    public Comment() {
    }

  
    public Comment(String message, long id, String autor) {
        this.message = message;
        this.id = id;
        this.autor = autor;
        this.created=new Date();
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }
  
  
  
}
