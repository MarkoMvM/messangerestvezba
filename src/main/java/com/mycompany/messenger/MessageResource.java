
package com.mycompany.messenger;

import com.mycompany.messenger.model.Message;
import com.mycompany.messenger.service.MessageService;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;

import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

@Path("/message")
public class MessageResource {
    MessageService messageServices= new MessageService();
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List message(@QueryParam("year") int year,
                        @QueryParam("start") int start,
                        @QueryParam("size") int size){
        if(year > 0 ){
            return messageServices.getAllMessageForYear(year);
        }
        if(start >= 0 && size >= 0){
            return messageServices.getAllMessagePaginated(start, size);
        }
        return messageServices.getAllMessages();
    }
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Message addMessage(Message message){
       return messageServices.addMessage(message); 
    }
    
    @PUT
    @Path("/{messageId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Message updateMessage(@PathParam("messageId") long messageId , Message message){
        message.setId(messageId);
        return messageServices.updateMessage(message);
    }
    @DELETE
    @Path("/{messageId}")
    @Produces(MediaType.APPLICATION_JSON)
    public void delete(@PathParam("messageId") long messageId){
        messageServices.removeMessage(messageId);
    
    }
    @GET
    @Path("/{messageId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Message test(@PathParam("messageId") long messageId){
        return messageServices.getMessage(messageId);
    }
    @Path("/{messageId}/comments")
    public CommentsResource getComments(){
        return new CommentsResource();
    }
    
}
