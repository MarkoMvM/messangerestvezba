
package com.mycompany.messenger;

import com.mycompany.messenger.model.Comment;
import com.mycompany.messenger.service.CommentsService;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

@Path("/")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class CommentsResource {
    private CommentsService commentServices = new CommentsService();
    
    @GET
    public List<Comment> getAllComments(@PathParam ("messageId") long messageId){
           return  commentServices.getAllComments(messageId);
    
    }
    @POST
    public Comment addMessage(@PathParam ("messageId") long messageId,Comment comment){
       return commentServices.addComment(messageId, comment);
    }
    @PUT
    @Path("/{commentId}")
    public Comment updateComment(@PathParam("messageId") long messageId,@PathParam("commentId")long commentId,Comment comment){
        comment.setId(commentId);
        return commentServices.updateComment(messageId, comment);
    }
    
    @DELETE
    public void deleteComments(@PathParam("messageId") long messageId,@PathParam("commentId")long commentId,Comment comment){
        commentServices.removeComment(commentId, messageId);
    }
    @GET
    @Path("/{commentId}")
    public Comment getMessage(@PathParam("commentId") long commentId){
        return commentServices.getComment(commentId, commentId);    
    }
}
