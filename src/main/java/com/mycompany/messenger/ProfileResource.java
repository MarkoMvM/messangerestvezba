
package com.mycompany.messenger;

import com.mycompany.messenger.model.Profile;
import com.mycompany.messenger.service.ProfileService;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("profile")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ProfileResource {
    
    ProfileService profileServices = new ProfileService();
    
    
    @GET
    public List<Profile> getAllProfiles(){
      return  profileServices.getAllProfiles();
    }
    @POST
    public Profile addProfile(Profile profile){
       return profileServices.addProfile(profile);
    }
    @GET
    @Path("{profileName}")
    public Profile getProfile(@PathParam("profileName")String profileName){
        return profileServices.getProfil(profileName);
    }
    @PUT
    @Path("{profileName}")
    public Profile updatProfile(@PathParam("profileName")String profileName,Profile profile){
        profile.setProfilName(profileName);
        return profileServices.updateProfile(profile);
    }
    @DELETE
    @Path("{profileName}")
    public void deleteProfile(@PathParam("profileName") String profileName){
        profileServices.removeProfile(profileName);
        
    }
}
